(function () {
    var m = window.location.pathname.match(/^\/school\/classroom\/tests\/([^/]+)$/);
    if (m) {
        setTimeout(function () {
            new Test(m[1]);
        }, 0);
    }

    function Test(testId) {

        var builderEnabled = false,
            total = 0,
            locked = 0;

        $(document).ajaxComplete(function (event, request, settings) {
            if (!~settings.url.indexOf('checkTest.php')) {
                return;
            }
            try {
                var response = JSON.parse(request.responseText);
                if (response.fullResult == 0) {
                    if (!builderEnabled) {
                        enableBuilder();
                    }
                }
                else if (builderEnabled) {
                    removeBuilder();
                }
            }
            catch (err) {
            }
        });

        function enableBuilder() {

            builderEnabled = true;

            $('table.test').css({display: ''});
            $($('table.test > tbody > tr > td')[3]).append('<div id="sticker"></div>');
            $('#sticker').sticky({topSpacing: 20});

            $('.question')
                .each(function () {
                    var q = $(this);
                    var id = q.data('id');
                    $(this).append(
                        '<div class="lock-checkbox">'
                        + '<label style="float: right;">'
                        + '<input type="checkbox" class="lock ce-ls" data-ce-id="' + id + '"> Готово'
                        + '</label>'
                        + '</div>'
                    );
                    total++;
                });

            $('input.lock').change(function () {
                var button = $(this);
                var checked = button.prop('checked');
                var id = button.data('ce-id');
                var q = $('.question[data-id=' + id + ']');
                if (checked) {
                    locked++;
                    q.addClass('locked');
                    if ($('#hide_done').prop('checked')) {
                        q
                            .css({position: 'relative'})
                            .animate({height: 0}, 500, function () {
                                q.css({position: '', height: ''}).hide();
                            });
                    }
                }
                else {
                    locked--;
                    q.removeClass('locked');
                }

                updateSticker();
            });

            $('#sticker')
                .html(
                    '<div class="progress-wrap progress">'
                    + '<div class="progress-bar progress"></div>'
                    + '<div class="progress-label">Готово <span id="counters"></span></div>'
                    + '</div>'
                    + '<div><label><input type="checkbox" class="ce-ls" id="hide_done" data-ce-id="hide_done'
                    + testId + '" /> Скрыть готовые</label></div>'
                );

            $('#hide_done').change(function () {
                var checked = $(this).prop('checked');
                if (checked) {
                    $('.question.locked').hide();
                }
                else {
                    $('.question').show();
                }
            });

            $(window).resize(function () {
                updateProgressBar();
            });

            updateSticker();
            initLS();
        }

        function removeBuilder() {
            $('#sticker').hide();
            $('.question').show();
            $('.lock-checkbox').hide();
            $('.question.locked').removeClass('locked');
        }

        function updateSticker() {
            $('#counters').html(locked + ' из ' + total);
            updateProgressBar();
        }

        function updateProgressBar() {
            var percent = locked / total * 100;
            $('.progress-bar')
                .stop()
                .animate(
                    {
                        width: percent + '%',
                        'border-top-right-radius': percent > 80 ? 5 : 0,
                        'border-bottom-right-radius': percent > 80 ? 5 : 0
                    },
                    1000
                );
        }

        function initLS() {
            $('.ce-ls').each(function () {
                var e = 'ce-' + $(this).data('ce-id');
                var t = 0;
                $(this).on('click', function () {
                    t = 1 - t;
                    localStorage.setItem(e, t);
                });
                +localStorage.getItem(e) && $(this).click();
            });
        }

    }


})();
