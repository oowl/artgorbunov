

function inject(scriptName) {
    var s = document.createElement('script');
    s.src = chrome.extension.getURL(scriptName);
    s.onload = function() {
        this.remove();
    };
    (document.head || document.documentElement).appendChild(s);
}

inject('dist/jquery.sticky.js');
inject('dist/classroom.js');
inject('dist/test.js');
