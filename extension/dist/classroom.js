(function() {
    if (window.location.pathname.match(/^\/school\/classroom\/$/)) {
        new Classroom();
    }

    function Classroom() {

        var results = collect();
        //console.log(results);

        function collect() {
            var tests = [];

            $('a[href^="/school/classroom/tests/"]').each(function() {
                var href = this.href.match(/\/school\/classroom\/tests\/([^/]+)$/);
                var text = this.text.match(/\d+(?:\,\d+)?/g);
                var image = $(this).parents('li').find('.courseImage img');
                var course = image.length && image[0].src.match(/\/([^/]+?)(?:-bw)?\.png$/);
                if (href && text && text.length === 2 && course) {
                    tests.push([course[1], href[1], text[0], text[1]]);
                }
            });
            return tests;
        }

    }

})();
